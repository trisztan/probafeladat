## Előadók akik párban szerepelnek legalább 50 sorban.

## Követelmények:

    -   PHP >= 7.1.3
    -   BCMath PHP Extension
    -   Ctype PHP Extension
    -   JSON PHP Extension
    -   Mbstring PHP Extension
    -   OpenSSL PHP Extension
    -   PDO PHP Extension
    -   Tokenizer PHP Extension
    -   XML PHP Extension

## Telepítés:
    -   git clone ..
    -   átnevezni .env.example .env névre
    -   .env file-ban APP_URL= megadni a domain címet:  APP_URL=yourdomain.com
    -   composer install --prefer-dist
    -   php artisan key:generate

## Jogosultság beállítások
    -   chown -R [...]:[...] .
    -   chmod 777 -R storage
    -   chmod -R guo+w storage
    -   find . -type d -exec chmod 755 {} +
    -   find . -type f -exec chmod 655 {} + 
    
## Hogyan használd a weboldalt
    -   Url megnyitása
    -   Letöltés gomb
    -   Az eredmény a filteres_artists.csv-ben található
