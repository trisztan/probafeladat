#!/bin/sh
cd /var/www/vhosts/basicauth.com/artists.basicauth.com/
export PATH=/opt/plesk/php/7.1/bin:$PATH
git fetch --all
git reset --hard origin/master
php artisan down

echo "Run Composer Install"
composer install
echo "Clear cache"
php artisan cache:clear
echo "Fix permissions"
# Fix permissions
cd /var/www/vhosts/basicauth.com/artists.basicauth.com/
chown -R basicauth:psacln .
chmod 777 -R storage
chmod -R guo+w storage
find . -type d -exec chmod 755 {} +
find . -type f -exec chmod 655 {} +

php artisan up
