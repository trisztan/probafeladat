<?php

namespace App\Http\Controllers;

class IndexController extends Controller
{

    private $pairs = [];
    private $lines = [];
    private $read;
    private $write;

    public function __construct()
    {
        ini_set('memory_limit', '-1');
        $this->read = storage_path('artists.csv');
        $this->write = storage_path('filtered_artists.csv');
    }

    public function index()
    {
        return view('welcome');
    }

    public function findArtists()
    {
        $this->openCsv();
        return $this->writeCsv();
    }

    /**
     * Open csv
     */
    private function openCsv()
    {
        $file = fopen($this->read, 'r');
        while (($line = fgetcsv($file)) !== false) {
            $this->generatePairs($line);
        }
    }

    /**
     * Count and find pairs
     * @param $a
     */
    private function generatePairs($a)
    {
        foreach ($a as $k => $v) {
            foreach (array_slice($a, $k + 1) as $k2 => $v2) {
                $id = 'a' . md5($v . $v2);
                $id2 = 'a' . md5($v2 . $v);
                if (isset($this->lines[$id])) {
                    $this->lines[$id] = ['first' => $v, 'second' => $v2];
                    $this->pairs[$id] = ++$this->pairs[$id];
                    continue;
                }
                if (isset($this->lines[$id2])) {
                    $this->lines[$id2] = ['first' => $v2, 'second' => $v];
                    $this->pairs[$id2] = ++$this->pairs[$id2];
                    continue;
                }

                $this->lines[$id] = ['first' => $v, 'second' => $v2];
                $this->pairs[$id] = 1;
            }
        }
    }

    /**
     * Write lines to csv
     */
    private function writeCsv()
    {
        $fp = fopen($this->write, 'w+');
        foreach ($this->pairs as $id => $count) {
            if ($count <= 50) {
                continue;
            }

            fputcsv($fp, [$this->lines[$id]['first'], $this->lines[$id]['second']]);
        }
        fclose($fp);
        return response()->download($this->write)->deleteFileAfterSend();
    }

}
